import React, { useState } from 'react';
import Error from './Error';
import '../css/app.css';

import axios from 'axios';
import Swal from 'sweetalert2';
import { withRouter } from 'react-router-dom';

const NuevoRegistro = ({history, guardarRecargarRegistros}) => {

    const [userId, guardarAutor] = useState('')
    const [title, guardarContenido] = useState('')
    const [completed, guardarCompleto] = useState(true)
    const [error, guardarError] = useState(false)


    const AgregarRegistro = async e => {
        e.preventDefault();

        if (userId === '' || title === '') {
            guardarError(true)
            /* Interrupción de Ejecución */
            return;
        }

        guardarError(false)

        try {
            const resultado = await axios.post('http://localhost:4000/registros', { userId, title, completed })

            if (resultado.status === 201) {
                Swal.fire(
                    'Registro Creado!',
                    'Registro Creado Correctamente!',
                    'success'
                )
            }

        } catch (error) {
            console.log(error)

            Swal.fire({
                type: 'error',
                tittle: 'Error',
                text: 'Hubo un error en la creación, vuelve a intentarlo'
            })
        }

        /* Recargar Registros */
        guardarRecargarRegistros(true)
        /* Redirigir a Registros */
        history.push('/')

    }


    return (
        <div className="formNuevo">
            <form onSubmit={AgregarRegistro}>
                <span>First name:</span>
            <input
                    type="text"
                    name="autor"
                    placeholder="autor"
                    onChange={e => guardarAutor(e.target.value)}
                />

                <span>Last name:</span>
                <input
                    type="text"
                    name="contenido"
                    placeholder="contenido"
                    onChange={e => guardarContenido(e.target.value)}
                />
                {(error) ? <Error mensaje='Todos los campos son obligatorios'/> : null}
                <button type="submit" value="Submit">Crear</button>

            </form>
        </div>
    )

}

export default withRouter(NuevoRegistro);