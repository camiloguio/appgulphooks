import React, { Fragment } from 'react';
import Registro from './Registro';

const Registros = ({registros, guardarRecargarRegistros}) => {
    return (
        <Fragment>
            <ul className="lista">
                {registros.map(registro => (
                    <Registro 
                    registro = {registro}
                    guardarRecargarRegistros = {guardarRecargarRegistros}
                    key = {registro.id} 
                    />
                ))}
            </ul>
        </Fragment>
    );
};

export default Registros;