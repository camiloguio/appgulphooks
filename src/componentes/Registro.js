import React from 'react';
import { Link } from 'react-router-dom';

import Axios from 'axios';
import Swal from 'sweetalert2';

function Registro ({ registro, guardarRecargarRegistros }){

    const eliminarRegistro = id => {
        console.log('eliminado', id)
        /* Pop up de confirmación Borrado */
        Swal.fire({
            title: '¿Estas Seguro?',
            text: "Un registro eliminado no se puede recuperar",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar'
        }).then(
            async (result) => {
                if (result.value) {
                    try {
                        const url = `http://localhost:4000/registros/${id}`
                        const resultado = await Axios.delete(url)
                        console.log(resultado)
                        /* Pop up afirmación Borrado de Registro */
                        if (resultado.status === 200) {

                            Swal.fire(
                                'Eliminado',
                                'El producto se ha eliminado',
                                'success'
                            )
                            /* Refrescar API */
                            guardarRecargarRegistros(true)
                        }
                        /* Pop up Erroneo */
                    } catch (error) {
                        console.log(error)
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: 'Hubo un error al eliminar el registro, vuelve a intentarlo'
                        })
                    }
                }
            }
        )
    }

    return (
        <li className="itemRegistro">

            <div className="infoItem">
                <div><span>Autor: </span>{registro.userId}</div>
                <div className="textCtn">{registro.title}</div>
            </div>

            <div className="botones">

                <Link to={`/registros/editar/${registro.id}`}>
                    <button className="btnAf">Editar</button>
                </Link>

                <button
                    className="btnNeg"
                    type="button"
                    onClick={() => eliminarRegistro(registro.id)}
                >Eliminar
                </button>
            </div>
        </li>
    );
};

export default Registro;