import React, { useRef, useState } from 'react';
import Error from './Error';
import '../css/app.css';

import { withRouter } from 'react-router-dom';
import Axios from 'axios';
import Swal from 'sweetalert2';

function EditarRegistros(props) {

    const { history, registro, guardarRecargarRegistros } = props

    const [error, guardarError] = useState(false)

    /* Generando Refs */
    const autorRef = useRef('')
    const contenidoRef = useRef('')

    const EditarRegistro = async e => {
        e.preventDefault();        

        const nuevoAutor = autorRef.current.value,
            nuevoContenido = contenidoRef.current.value;

            if(nuevoAutor === '' || nuevoContenido === ''){
                guardarError(true)
                return
            }

            guardarError(false)

        const editarRegistro = {
            userId: nuevoAutor,
            title: nuevoContenido
        }

        // Enviar el Request
        const url = `http://localhost:4000/registros/${registro.id}`

        try {
            const resultado = await Axios.put(url, editarRegistro);

            if (resultado.status === 200) {
                Swal.fire(
                    'Producto Editado',
                    'El producto se editó correctamente',
                    'success'
                )
            }
        } catch (error) {
            console.log(error);
            Swal.fire({
                type: 'error',
                title: 'Error',
                text: 'Hubo un error, vuelve a intentarlo'
            })
        }

        guardarRecargarRegistros(true)
        history.push('/')

    }

    return (
        <div className="formNuevo">
            <form onSubmit={EditarRegistro}>

                <span>Autor:</span>
            <input
                    type="text"
                    name="autor"
                    placeholder="autor"
                    ref={autorRef}
                    defaultValue={(registro === undefined ? null : registro.userId)}
                />

                <span>Contenido:</span>
                <input
                    type="text"
                    name="contenido"
                    placeholder="contenido"
                    ref={contenidoRef}
                    defaultValue={(registro === undefined ? null : registro.title)}
                />
                {(error) ? <Error mensaje='Todos los campos son obligatorios'/> : null}

                <button type="submit" value="Submit">Editar</button>
            </form>
        </div>
    )
}

export default withRouter(EditarRegistros);