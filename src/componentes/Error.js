import React from 'react';
import '../css/app.css';

const Error = ({mensaje}) => {
    return (
        <p className="errorForm">{mensaje}</p>
    );
};

export default Error;