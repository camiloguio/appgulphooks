import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import '../css/app.css';

const Header = () => {

    return (
        <div className="header">

            <div className="marca">
                <Link to="/">
                    <h1 className="titulo">Crud HOOKs</h1>
                </Link>
            </div>

            <div className="menu">
                <ul>
                    <li>
                        <NavLink
                            to='/'
                            className="itemMenu"
                            activeClassName="active">
                            Listado de Registros
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            to='/nuevo-registro'
                            className="itemMenu"
                            activeClassName="active">
                            Crear Registro
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>

    );
};

export default Header;