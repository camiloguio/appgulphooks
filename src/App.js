import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import './css/app.css';
import axios from 'axios';
import Registros from './componentes/Registros';
import Header from './componentes/Header';
import EditarRegistros from './componentes/EditarRegistros';
import NuevoRegistro from './componentes/NuevoRegistro';

function App() {

  const [registros, guardarRegistros] = useState([])
  const [recargarRegistros, guardarRecargarRegistros] = useState(true)

  useEffect(() => {
    if (recargarRegistros) {
      const consultarApi = async () => {
        /* Petición para la consulta de la API */
        const resultado = await axios.get('http://localhost:4000/registros')
        guardarRegistros(resultado.data)
      }
      consultarApi()
      /* Cambiando el estado para recargar los productos */
      guardarRecargarRegistros(false)
    }
  }, [recargarRegistros])




  return (

    /* Router de la aplicación */

    <Router>
      <Header />
      <Switch>

        <Route exact path='/'
          render={() => (
            <div className="contenedor">
              <Registros 
              registros={registros}
              guardarRecargarRegistros = {guardarRecargarRegistros}
               />
            </div>
          )} />

        {/* Crear Registro */}
        <Route exact path="/nuevo-registro"
          render={() => (
            <NuevoRegistro guardarRecargarRegistros={guardarRecargarRegistros} />
          )}
        />

        {/* Editar los Registros */}
        <Route exact path="/registros/editar/:id"
          render={props => {
            /* Tomando el ID del Producto */
            const idRegistro = parseInt(props.match.params.id)

            /* Pasando el Producto al State */            
              let registro = registros.filter(registro => registro.id === idRegistro)

            return (

              <EditarRegistros
                registro={registro[0]}
                guardarRecargarRegistros = {guardarRecargarRegistros}

              />
            )

          }}
        />
      </Switch>
    </Router>

  );
}

export default App;
